import React, {Component} from 'react'

class Author extends Component{
  render(){
    return (<div>
      {this.props.author.name} a înregistrat urmatoarele date în jurnal: {this.props.author.record}
    </div>)
  }
}

export default Author