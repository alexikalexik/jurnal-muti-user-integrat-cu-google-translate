const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')

const sequelize = new Sequelize('rest_messages', 'root', '', {
  dialect : 'mysql',
  define : {
    timestamps : false
  }
})

const Journal = sequelize.define('journal', {
  name : {
    type : Sequelize.STRING,
    allowNull : false,
    validate : {
      len : [3,20]
    }
  },
  email : {
    type : Sequelize.STRING,
    allowNull : false,
    validate : {
      isEmail :true
    }
  }
}, {
  underscored : true
})

const Entry = sequelize.define('entry', {
  title  : {
    type : Sequelize.STRING,
    allowNull : false,
    validate : {
      len : [3,20]
    }
  },
  content : {
    type : Sequelize.TEXT,
    allowNull : false,
    validate : {
      len : [10,1000]
    }
  },
  timestamp : {
    type : Sequelize.DATE,
    allowNull : false,
    defaultValue : Sequelize.NOW
  }
})

Journal.hasMany(Entry)

const journalApp = express()
journalApp.use(bodyParser.json())

journalApp.get('/create', (req, res, next) => {
  sequelize.sync({force : true})
    .then(() => res.status(201).send('created'))
    .catch((error) => next(error))
})

journalApp.get('/journals' , (req, res, next) => {
  Journal.findAll()
    .then((journals) => res.status(200).json(journals))
    .catch((error) => next(error))
})

journalApp.post('/journals' , (req, res, next) => {
  Journal.create(req.body)
    .then(() => res.status(201).send('Journal succescreated'))
    .catch((error) => next(error))
})

journalApp.get('/journals/:id' , (req, res, next) => {
  Journal.findById(req.params.id, {include : [Entry]})
    .then((journal) => {
      if (journal){
        res.status(200).json(journal)
      }
      else{
        res.status(404).send('Journal not found')
      }
    })
    .catch((error) => next(error))
})

journalApp.put('/journals/:id' , (req, res, next) => {
  Journal.findById(req.params.id)
    .then((journal) => {
      if (journal){
        return journal.update(req.body, {fields : ['name', 'email']})
      }
      else{
        res.status(404).send('Journal not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('Journal successfully modified')
      }
    })
    .catch((error) => next(error))
})

journalApp.delete('/journals/:id' , (req, res, next) => {
    Journal.findById(req.params.id)
    .then((journal) => {
      if (journal){
        return journal.destroy()
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('modified')
      }
    })
    .catch((error) => next(error))
})

journalApp.get('/journals/:aid/entrys' , (req, res, next) => {
  Journal.findById(req.params.aid)
    .then((journal) => {
      if (journal){
        return journal.getEntrys()
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then((entrys) => {
      if (!res.headersSent){
        res.status(200).json(entrys)
      }
    })
    .catch((error) => next(error))
})

journalApp.post('/journals/:aid/entrys' , (req, res, next) => {
  Journal.findById(req.params.aid)
    .then((journal) => {
      if (journal){
        let entry = req.body
        entry.author_id = journal.id
        return Entry.create(entry)
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('created')
      }
    })
    .catch((error) => next(error))  
})

journalApp.get('/journals/:aid/entrys/:mid' , (req, res, next) => {
  Journal.findById(req.params.mid, {
      where : {
        journal_id : req.params.aid
      }
    })
    .then((entry) => {
      if (entry){
        res.status(200).json(entry)
      }
      else{
        res.status(404).send('not found')
      }
    })
    .catch((error) => next(error))  
})

journalApp.put('/journals/:aid/entrys/:mid' , (req, res, next) => {
  Journal.findById(req.params.mid)
    .then((entry) => {
      if (entry){
        return entry.update(req.body, {fields : ['title', 'content']})
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('modified')
      }
    })
    .catch((error) => next(error))
})
journalApp.delete('/journals/:aid/entrys/:mid' , (req, res, next) => {
  Journal.findById(req.params.mid)
    .then((entry) => {
      if (entry){
        return entry.destroy()
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('modified')
      }
    })
    .catch((error) => next(error))
})

journalApp.use((err, req, res, next) =>{
  console.warn(err)
  res.status(500).send('some error')
})

journalApp.listen(8080);