# README #

Acest README cuprinde o descriere a aplicatiei si date despre limbajele folosite

### Tema proiectului ###

* Realizarea unei aplicații cu tema: Jurnal multi-user integrat cu Google Translate, 
* cu back-end RESTful sau java care accesează date stocate într-o bază 
* relațională pe baza unui API de persistenţă și date expuse de un serviciu extern 
* și front-end SPA realizat cu un framework bazat pe componente.

### Descrierea proiectului  ###

* Prin intermediul paginii home se va putea face un login la un cont existent sau se
* va putea creea un nou cont pentru a accesa un o pagina cu toate jurnalele existente.
* Dupa aceasta operatiune utilizatorul va putea sa creeze un jurnal sau sa il editeze
* pe cel creeat anterior. Deoarece utilizatorii pot avea si alta nationalitate decat
* se va integra prin intermediul Google translate traducerea paginii in limba
* utilizatorului curent.

### Continutul jurnalelor  ###

* Pentru o apartenenta cat mai buna fiecare utilizator va aduce referire la o anumita
* tema urmand mai apoi ca ceilalti sa adauge sau sa faca adnotari la continutul 
* acestuia cu permisiunea celui care a facut jurnalul initial
