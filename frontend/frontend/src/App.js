import React, { Component } from 'react'
import './App.css'
import './logo.svg'
import AuthorList from './AuthorList'

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Jurnal multi-user integrat cu google translate</h1>
        </header>
        
        <AuthorList />
      </div>
      
      
    )
  }
}

export default App
