import React, {Component} from 'react'

class AuthorForm extends Component{
  constructor(props){
    super(props)
    this.state = {
      authorName : '',
      authorRecord : ''
    }
    this.handleChange = (event) => {
      this.setState({
        [event.target.name] : event.target.value
      })
    }
  }
  render(){
    return (<form>
        Name : <input type="text" onChange={this.handleChange} name="authorName" />
        Record : <input type="text"onChange={this.handleChange} name="authorRecord" />
        <input type="button" value="Adauga înregistrare" onClick={() => this.props.onAdd({name : this.state.authorName, record : this.state.authorRecord})} />
      </form>)
  }
}

export default AuthorForm