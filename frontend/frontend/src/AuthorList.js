import React, {Component} from 'react'
import Author from './Author'
import AuthorForm from './AuthorForm'

class AuthorList extends Component{
  constructor(props){
    super(props)
    
    this.addAuthor = (author) => {
      let authorState = this.state.authors
      authorState.push(author)
      this.setState({
        authors : authorState
      })
    }
    
    this.state = {
      authors : [{name : 'Harabagiu Alexandru', record : 'Astăzi 18.01 am adăugat prima pagină a jurnalului'},{name : 'Marcel', record : 'Astăzi 01.01 am băut ca porcul'}],
    }
  }
  render(){
    return (<div>
      {
        this.state.authors.map((a) => 
          <div>
            <Author author={a} />
          </div>
        )
      }
     <AuthorForm onAdd={this.addAuthor}/>
    </div>)
  }
}

export default AuthorList